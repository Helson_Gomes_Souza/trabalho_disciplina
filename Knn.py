'''
Nesse script irei gerar uma base de dados com informações aleatórias a fim de fazer uma predição
usando machine learning por meio do algorítmo do vizinho mais próximo (knn)

'''

#Pacotes utilizados

import matplotlib.pyplot as plt
import sklearn
import pandas as pd
import numpy as np

#Aqui vamos criar o data frame
#np.random.seed(0)
#data = {'notas': np.random.randint(3, 10, 250), 'faltas' : np.random.randint(0, 100, 250), 'renda': np.random.randint(0, 2500, 250), 'dist' : np.random.randint(1, 30, 250), 'branco': np.random.randint(0, 1, 250)}

from sklearn.datasets import load_iris
dados = load_iris()
print(dados)

#Treinando a maquina
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(dados['data'], dados['target'], random_state = 0)

#Criando os vizinhos
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors = 1)

#usando o knn.fit
knn.fit(X_train, y_train)
KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski', metric_params=None, n_jobs=1, n_neighbors=1, p=2, weights='uniform')

#Predição
X_new = np.array([5, 2.9, 1, 0.2])
X_new = X_new.reshape(1, -1)
predict = knn.predict(X_new)
dados['target_names'][predict]

knn.score(X_test, y_test)
